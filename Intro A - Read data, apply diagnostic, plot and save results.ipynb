{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Dynlib introduction: Read data, apply diagnostic, plot and save results\n",
    "\n",
    "In this notebook we will: \n",
    " - Read ERA5 data\n",
    " - Apply a diagnostic function\n",
    " - Plot the results\n",
    " - Save the results back to disk\n",
    "\n",
    "We will first do this for the case study of Dagmar, and then demonstrate of how to apply the diagnostic for (parts of) the entire ERA5-period (currently: 1979-2020). "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Some general imports we will need in any case."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "\n",
    "from dynlib.metio.era5 import conf, dt, get_instantaneous, metsave\n",
    "\n",
    "import dynlib.diag\n",
    "import dynlib.thermodyn\n",
    "import dynlib.proj as proj\n",
    "import dynlib.figures as fig"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Case study: Dagmar"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Get temperature and humidity on 850 hPa for Ekstremværet Dagmar\n",
    "plev = '850'\n",
    "timeinterval = [dt(2011,12,25,0), dt(2011,12,26,12)]\n",
    "\n",
    "dat, grid = get_instantaneous([(plev, 't'), (plev,'q'), (plev, 'u'), (plev,'v'), ], timeinterval)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now let's explore what we have got. First the data structure itself."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "type(dat), dat.keys()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Then the grid object."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "type(grid)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "What does it contain?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Some useful information in the grid object\n",
    "print('Time info:')\n",
    "print(grid.t_name, grid.t, grid.t_parsed[0], grid.t_parsed[-1])\n",
    "print()\n",
    "print('Info about coordinates:')\n",
    "print(grid.x_name, grid.x.shape, grid.x.min(), grid.x.max())\n",
    "print()\n",
    "print('Including grid spacing (in dynlib convention! -- 2dx centered on grid cell in question)')\n",
    "print(grid.dx.shape, grid.dx.min(), grid.dx.max())\n",
    "print(grid.dy.shape, grid.dy.min(), grid.dy.max())"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Calculate equivalent potential temperature\n",
    "# thetae_from_q has specific humidity, temperature and pressure as arguments -> need to create a pres array.\n",
    "pres = np.ones(dat[plev,'q'].shape) * 850.0e2\n",
    "eqpt = dynlib.thermodyn.thetae_from_q(dat[plev,'q'][:,0,:,:], dat[plev,'t'][:,0,:,:], pres)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "A quick sanity check of what we got here:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "eqpt.shape, eqpt.min(), eqpt.max()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Values look plausible, let's continue to calculate frontogenesis."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Calculate adiabatic eqpt frontogenesis due to horizontal winds\n",
    "#\n",
    "# The function calculates also frontogenesis due to heating and \n",
    "# tilting. We're not intrested in these terms and set these inputs \n",
    "# to zero / vertical distances to one\n",
    "zeros = np.zeros(dat[plev,'u'].shape)\n",
    "ones = np.ones(dat[plev,'u'].shape)\n",
    "eqpt_frg_div, eqpt_frg_def, ___, ___ = dynlib.diag.frontogenesis_contributors(\n",
    "    dat[plev,'u'], dat[plev,'v'], zeros, eqpt[:,np.newaxis,:,:], zeros, grid.dx, grid.dy, ones)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Note the 4d-input for this function! That is because the function is meant to work with data that has both a time and a vertical dimension.\n",
    "\n",
    "Now what did we get as a result?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "eqpt_frg_def.shape, eqpt_frg_def.min(), eqpt_frg_def.max()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Whoops, we got some NaNs! Is this a serious issue?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "np.isnan(eqpt_frg_def).sum(), eqpt_frg_def.size"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Oh, ok. Less than 1% NaNs can be expected. For example, there is no meridional derivative at the poles, leading to NaNs for these grid points."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "np.nanmin(eqpt_frg_def), np.nanmax(eqpt_frg_def)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Looks good. So let's save these results, and finally do some plotting."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# grid does not contain vertical level information (as data from different levels could have been requested)\n",
    "# -> Inject vertical level information required for saving\n",
    "ogrid = grid.new_plev(plev)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Save results as netCDF file\n",
    "tosave = {\n",
    "    'eqpt_frg_div': eqpt_frg_div,\n",
    "    'eqpt_frg_def': eqpt_frg_def,\n",
    "}\n",
    "metsave(tosave, ogrid, f'ea.ans.Dagmar.{plev}.eqpt_frontogenesis')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "I suggest to take a look at the netCDF file we just created with ncdump and ncview before proceeding.\n",
    "\n",
    "You'll see that the data is automatically compressed from 8-byte float to 2-byte integers to save disk space. This can of course be disabled for cases when this is not desirable."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%matplotlib inline\n",
    "# Finally some plotting\n",
    "# Plot results\n",
    "for tidx in [0, 4, 8]: # plot only a few selected time steps\n",
    "    fig.map(eqpt_frg_def[tidx,0,...], grid, q='eqpt_frg_def', plev=plev, name=grid.t_parsed[tidx], \n",
    "            m=proj.N_Atlantic, cmap=\"RdGy_r\", scale=np.arange(-7.5,7.6,1)*1.0e-9)\n",
    "    fig.plt.close()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now these fields look interesting. What about a more climatological approach?\n",
    "\n",
    "## Climatological frontogenesis in the North Atlantic"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The ERA5 data that we have is stored in monthly chunks, so this is a natural rhythm for doing the analyses. Also, we can then directly be a bit selective with the months we take into account."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# All ERA5\n",
    "#years = range(1979,2021)\n",
    "#months = range(1,13)\n",
    "\n",
    "# Winter months of 2020\n",
    "years = range(2020,2021)\n",
    "months = [1,2,12]\n",
    "\n",
    "periods = [\n",
    "    (dt(yr, mon,1), dt(yr + mon // 12, mon % 12 + 1, 1))\n",
    "    for yr in years for mon in months\n",
    "]\n",
    "print(len(periods))\n",
    "print(periods[0])\n",
    "print(periods[-1])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now that we have a list of time intervals, we can just loop over the analysis as we did it before:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Result arrays\n",
    "tsteps = 0\n",
    "mean_frg_div = np.zeros(conf.gridsize)\n",
    "mean_frg_def = np.zeros(conf.gridsize)\n",
    "\n",
    "# Set these to a convenient location if necessary\n",
    "SAVE_RESULTS = False\n",
    "conf.opath = '.'\n",
    "\n",
    "for period in periods:\n",
    "    # Get the data\n",
    "    dat, grid = get_instantaneous([(plev, 't'), (plev,'q'), (plev, 'u'), (plev,'v'), ], period)\n",
    "    \n",
    "    # Diagnose eqpt\n",
    "    pres = np.ones(dat[plev,'q'].shape) * 850.0e2\n",
    "    eqpt = dynlib.thermodyn.thetae_from_q(dat[plev,'q'][:,0,:,:], dat[plev,'t'][:,0,:,:], pres)\n",
    "    \n",
    "    # Calculate frontogenesis function\n",
    "    zeros = np.zeros(dat[plev,'u'].shape)\n",
    "    ones = np.ones(dat[plev,'u'].shape)\n",
    "    eqpt_frg_div, eqpt_frg_def, ___, ___ = dynlib.diag.frontogenesis_contributors(\n",
    "            dat[plev,'u'], dat[plev,'v'], zeros, eqpt[:,np.newaxis,:,:], zeros, grid.dx, grid.dy, ones)\n",
    "    \n",
    "    mean_frg_div += eqpt_frg_div[:,0,:,:].sum(axis=0)\n",
    "    mean_frg_def += eqpt_frg_def[:,0,:,:].sum(axis=0)\n",
    "    tsteps += eqpt_frg_div.shape[0]\n",
    "    \n",
    "    # Optional: save. In this make sure to have conf.opath set to a convenient location!\n",
    "    if SAVE_RESULTS:\n",
    "        date = period[0] # start of the time interval\n",
    "        periodstr = f'{date.year:04d}{date.month:02d}'\n",
    "        ogrid = grid.new_plev(plev)\n",
    "        tosave = {\n",
    "            'eqpt_frg_div': eqpt_frg_div,\n",
    "            'eqpt_frg_def': eqpt_frg_def,\n",
    "        }\n",
    "        metsave(tosave, ogrid, f'ea.ans.{periodstr}.{plev}.eqpt_frontogenesis')\n",
    "\n",
    "# From sum to average\n",
    "mean_frg_div /= tsteps\n",
    "mean_frg_def /= tsteps"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Just a cursory check of what we got ... "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "mean_frg_div.shape, np.nanmin(mean_frg_div), np.nanmax(mean_frg_div)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Then onto some plotting."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Plot frontogenesis due to deformation\n",
    "fig.map(mean_frg_def, grid, q='eqpt_frg_def', plev=plev, title='Mean Eqpt frontogenesis due to deformation', \n",
    "            m=proj.N_Atlantic, cmap=\"RdGy_r\", scale=np.arange(-7.5,7.6,1)*1.0e-10)\n",
    "fig.plt.close()\n",
    "\n",
    "# Plot frontogenesis due to divergence/convergence\n",
    "fig.map(mean_frg_div, grid, q='eqpt_frg_div', plev=plev, title='Mean Eqpt frontogenesis due to divergence', \n",
    "            m=proj.N_Atlantic, cmap=\"RdGy_r\", scale=np.arange(-7.5,7.6,1)*1.0e-10)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "dynpie3-2021a",
   "language": "python",
   "name": "dynpie3-2021a"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.0"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}

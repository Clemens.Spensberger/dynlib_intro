{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Dynlib introduction: Reference scripts using the feature detection algorithms\n",
    "\n",
    "In this notebook we will: \n",
    " - Detect cyclones and anticyclones using the Wernli and Schwierz (2006)-algorithm\n",
    " - Detect frontal volumes\n",
    " - Detect moisture transport axes\n",
    " - Detect jet axes\n",
    " - Detect Rossby wave breaking using the Rivère (2007)-algorithm\n",
    " - Detect blocking by gradient reversal\n",
    " - Detect cold-air outbreaks / calculate the cold-air outbreak index\n",
    " - Plot everything on one plot (not all features will appear)\n",
    " \n",
    "We will first do this for the case study of Dudley-Eunice clustering case in February 2022."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "\n",
    "from dynlib.metio.era5 import conf, dt, get_instantaneous, metopen\n",
    "from dynlib import dynfor, proj\n",
    "\n",
    "import dynlib.detect\n",
    "import dynlib.sphere\n",
    "import dynlib.thermodyn\n",
    "import dynlib.figures as fig\n",
    "\n",
    "\n",
    "# General configuration\n",
    "period = [dt(2022,2,10), dt(2022,2,20)] # Time period of the Dudley-Eunice-Franklin case\n",
    "dynfor.config.grid_cyclic_ew = True     # Domain is periodic in x"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Cyclone detection"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Load data, only sea-level pressure required\n",
    "plevqs = [('sfc','msl'),]\n",
    "dat, grid = get_instantaneous(plevqs, period)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Set thresholds and do the actual detection\n",
    "# Thresholds all based on Sprenger et al. (2017), doi: 10.1175/BAMS-D-15-00299.1\n",
    "dynfor.config.min_dist = 750       # in km; Minimum distance between two cyclone centres\n",
    "dynfor.config.min_size = 0.5e6     # in km^2; Minimum size of cyclone mask\n",
    "dynfor.config.max_size = 4.5e6     # in km^2; Maximum size of cyclone mask\n",
    "dynfor.config.max_oro = 1500       # in m; Maximum orographic height over which cyclone centers are detected\n",
    "dynfor.config.min_prominence = 200 # in Pa; Minimum difference between outermost contour and SLP minimum\n",
    "\n",
    "cycmask, meta = dynlib.detect.cyclone_by_contour(dat['sfc','msl'], grid)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Exploring cycmask\n",
    "print(cycmask.shape)               # shape (t,y,x)\n",
    "print(cycmask.max())               # how many different cyclones have been detected? \n",
    "print((cycmask > 0).sum()/cycmask.size)  # fraction of grid points part of any cyclone"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Exploring meta\n",
    "print(meta.shape)                  # shape (t,cyc_id,info)\n",
    "\n",
    "# For every cyclone we got a 5 bits of meta information:\n",
    "print(meta[0,0,:2])                # latitude [deg], longitude [deg]\n",
    "print(meta[0,0,2:4])               # min. pressure [Pa], outer contour pressure [Pa]\n",
    "print(meta[0,0,4])                 # cyclone size [km^2]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Anticyclone detection"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Set thresholds and do the actual detection\n",
    "# Thresholds all based on Sprenger et al. (2017), doi: 10.1175/BAMS-D-15-00299.1\n",
    "dynfor.config.min_dist = 750       # in km; Minimum distance between two anticyclone centres\n",
    "dynfor.config.cyc_minsize = 1.8e6  # in km^2; Minimum size of cyclone mask\n",
    "dynfor.config.cyc_maxsize = 18.0e6 # in km^2; Maximum size of cyclone mask\n",
    "dynfor.config.max_oro = 1500       # in m; Maximum orographic height over which cyclone centers are detected\n",
    "dynfor.config.min_prominence = 100 # in Pa; Minimum difference between outermost contour and SLP minimum\n",
    "\n",
    "# Detection algrithm is the same, we only invert the input field :)\n",
    "acycmask, meta = dynlib.detect.cyclone_by_contour(-dat['sfc','msl'], grid)\n",
    "\n",
    "# Return values thus also analogous to cyclone detection."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Front volumes"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Load data, only sea-level pressure required\n",
    "qs = ['t', 'q']\n",
    "plevs = ['925', '850', '700']\n",
    "plevqs = [(plev,q) for plev in plevs for q in qs]\n",
    "\n",
    "dat, grid = get_instantaneous(plevqs, period)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Load orography\n",
    "f, oro = metopen(conf.staticfile, 'oro', no_static=True)\n",
    "oro /= 9.81 # Convert from m^2/s^2 to m"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Threshold for ERAi based on Spensberger and Sprenger (2018), generalised to other \n",
    "# data sets based on the (global) 87.5-percentile of the thetae-gradient field at 850 hPa\n",
    "#\n",
    "# This generalisation does not work for CESM (and potentially other datasets), \n",
    "# because tropical gradients are systematically too weak. Thus generalisation based \n",
    "# on the extratropical percentile will be preferable.\n",
    "\n",
    "#dynfor.config.tfp_mingrad_largescale = 4.5e-5   # 4.5 K / 100 km Theta_E gradient for ERAi\n",
    "#dynfor.config.tfp_mingrad_largescale = 5.0e-5   # 5.0 K / 100 km Theta_E gradient for CERA-SAT\n",
    "dynfor.config.tfp_mingrad_largescale = 5.3e-5   # 5.3 K / 100 km Theta_E gradient for ERA5\n",
    "dynfor.config.tfp_grad_minsize = 75000.0e6      # 75000 km^2 minimum area per level\n",
    "\n",
    "# We got a bunch of single-level arrays which we need to assemble to one array including \n",
    "# all levels. We do this in combination with calculating eqpt from q and t\n",
    "s = dat['850','t'].shape\n",
    "eqpt = np.empty(s[:1]+(3,)+s[2:])\n",
    "for pidx, plev in enumerate(plevs):\n",
    "    p = np.ones(dat[plev,'q'].shape) * int(plev) * 100\n",
    "    eqpt[:,pidx,:,:] = dynlib.thermodyn.thetae_from_q(dat[plev,'q'], dat[plev,'t'], p)\n",
    "\n",
    "# TODO: Mask eqpt under ground\n",
    "    \n",
    "# The actual detection\n",
    "frovo_id = dynlib.detect.frontalvolume_largescale(eqpt, grid.dx, grid.dy, mountain_mask=oro > 1500)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Exploring frovo_id; very much analogous to cycmask above\n",
    "print(frovo_id.shape)              # shape (t,p,y,x)\n",
    "print(frovo_id.max())              # how many different front volumes have been detected? \n",
    "print((frovo_id > 0).sum()/frovo_id.size)  # fraction of grid points part of any front volume"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Moisture transport axes"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plevqs = [('sfc','viewvf'), ('sfc','vinwvf')]\n",
    "dat, grid = get_instantaneous(plevqs, period)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# The detection works best with data filtered to a lower spectral resolution, typical T84\n",
    "trunc = 84\n",
    "\n",
    "# Spectral analysis\n",
    "br, bi, cr, ci = dynlib.sphere.sh_analysis_vector(dat['sfc','viewvf'], dat['sfc','vinwvf'])\n",
    "\n",
    "# Triangular truncation\n",
    "dynlib.sphere.trunc_triangular(br, bi, trunc)\n",
    "dynlib.sphere.trunc_triangular(cr, ci, trunc)\n",
    "    \n",
    "#Spectral synthesis\n",
    "viewvfs, vinwvfs = dynlib.sphere.sh_synthesis_vector(dat['sfc','viewvf'].shape[2], br, bi, cr, ci)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "dynfor.config.nsmooth = 0                      # No additional smoothing (data to be filtered to T84)\n",
    "dynfor.config.searchrad = 1.5                  # Search radius in grid points, max distance between to points along a line\n",
    "#dynfor.config.jetint_thres = 2.04e-7           # e2 sfc T84\n",
    "#dynfor.config.jetint_thres = 4.03e-7           # et sfc T84\n",
    "dynfor.config.jetint_thres = 4.06e-7           # ea sfc T84\n",
    "\n",
    "# The algorithm is the same as the jet detection, only the jetint_thres-threshold is different\n",
    "mtaxis, mtaoff = dynlib.detect.jetaxis(5000,200, viewvfs,vinwvfs, grid.dx,grid.dy)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The arrays returned by the jet axis detection take a little bit of explanation."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(mtaxis.shape)            # shape (t, point index, info)\n",
    "print(mtaxis[0,0,:2])          # Fractional grid poing indices in x the and y directions, respectively.)\n",
    "print(mtaxis[:,:,0].max(), mtaxis[:,:,1].max()) # Note the Fortran indexing, i.e. first index is 1\n",
    "print(mtaxis[0,0,2])           # The total colum water vapour transport (=IVT) at the location of the axis\n",
    "print(mtaxis[:,:,2].max())"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(mtaoff.shape)            # shape (t, line index)\n",
    "print(mtaoff[0,:10])           # Gives the point indexes of the first point of the first 10 lines\n",
    "print(mtaxis[0,121:150,2])     # Thus: this is the IVT values along the third line of the first time step"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Jet axes"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# The procedure is very much analogous to the detection of moisture transport axes (or actually, vice versa :P)\n",
    "# We only request u and v on PV2 rather than the vertically integrated water vapour transport\n",
    "plevqs = [('pv2000','u'), ('pv2000','v')]\n",
    "dat, grid = get_instantaneous(plevqs, period)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# The detection works best with data filtered to a lower spectral resolution, typical T84\n",
    "trunc = 84\n",
    "\n",
    "# Spectral analysis\n",
    "br, bi, cr, ci = dynlib.sphere.sh_analysis_vector(\n",
    "    dat['pv2000','u'][:,0,:,:], \n",
    "    dat['pv2000','v'][:,0,:,:]\n",
    ")\n",
    "\n",
    "# Triangular truncation\n",
    "dynlib.sphere.trunc_triangular(br, bi, trunc)\n",
    "dynlib.sphere.trunc_triangular(cr, ci, trunc)\n",
    "    \n",
    "#Spectral synthesis\n",
    "us, vs = dynlib.sphere.sh_synthesis_vector(dat['pv2000','u'].shape[3], br, bi, cr, ci)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "dynfor.config.nsmooth = 0                      # No additional smoothing (data to be filtered to T84)\n",
    "dynfor.config.searchrad = 1.5                  # Search radius in grid points, max distance between to points along a line\n",
    "#dynfor.config.jetint_thres = 0.443e-8          # e2 pv2 T84\n",
    "#dynfor.config.jetint_thres = 0.55e-8           # ei pv2 T84\n",
    "dynfor.config.jetint_thres = 0.732e-8          # ea pv2 T84\n",
    "\n",
    "# The algorithm is the same as the jet detection, only the jetint_thres-threshold is different\n",
    "ja, jaoff = dynlib.detect.jetaxis(5000,200, us,vs, grid.dx,grid.dy)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The ja and jaoff arrays work equivalently to the mtaxis and mtaoff arrays discussed earlier. Only the third dimension of the ja array contains wind speed rather than the absolute water vapour transport."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ff = ja[:,:,2]\n",
    "ff = ff[ff > 0.0] # Removing those parts of the array that do not belong to any jet axis\n",
    "print(ff.mean(), ff.std())"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Rossby wave breaking"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plevqs = [('500','z'),]\n",
    "dat, grid = get_instantaneous(plevqs, period)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "rwb_a, rwb_c = dynlib.detect.rwb_by_contour(dat['500','z'], 'Z5', grid.x[0,:], grid.y[:,0], 21, grid.dx, grid.dy)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# The detection returns 0/1 mask fields marking the overturning parts of Z500-contours\n",
    "print(rwb_a.max(), rwb_c.max(), rwb_a.mean(), rwb_c.mean())"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Blocking by gradient reversal"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The algorithm can be applied to different input data. Here we use 250-hPa geopotential as the example."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plevqs = [('250','z'),]\n",
    "dat, grid = get_instantaneous(plevqs, period)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "min_duration = 41 # 41 time steps is 5 days at 3-hourly resolution\n",
    "dynfor.config.block_dj = int(round(15.0/abs(grid.y[1,0]-grid.y[0,0]))) # 15 deg lat \n",
    "dynfor.config.block_di = int(round(7.5/abs(grid.x[0,1]-grid.x[0,0])))  # 7.5 deg lon\n",
    "\n",
    "# Do the actual detections (NH, grid point boundaries most closely corresponding to 30-70N)\n",
    "idx0 = np.argmin(np.abs(grid.y[:,0]-30))\n",
    "idx1 = np.argmin(np.abs(grid.y[:,0]-70))\n",
    "lat0, lat1 = grid.y[idx0,0], grid.y[idx1,0]\n",
    "blockmask = dynlib.detect.block_by_grad_rev(dat['250','z'].squeeze(), grid, \n",
    "                                            min_duration=min_duration, lat_band=(lat0, lat1))\n",
    "\n",
    "# ... and for the SH (grid point boundaries most closely corresponding to 30-70S)\n",
    "idx0 = np.argmin(np.abs(grid.y[:,0]+30))\n",
    "idx1 = np.argmin(np.abs(grid.y[:,0]+70))\n",
    "if idx0 > idx1:\n",
    "    idx1, idx0 = idx0, idx1\n",
    "lat0, lat1 = grid.y[idx0,0], grid.y[idx1,0]\n",
    "blockmask_SH = dynlib.detect.block_by_grad_rev(-dat['250','z'].squeeze(), grid, \n",
    "                                               min_duration=min_duration, lat_band=(lat0, lat1))\n",
    "\n",
    "# Merge results\n",
    "blockmask[:,idx0:idx1,:] = blockmask_SH[:,idx0:idx1,:]\n",
    "del blockmask_SH"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The algorithm returns a binary mask field, marking by 1 where a block is being detected."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(blockmask.shape, blockmask.min(), blockmask.max(), blockmask.sum()/blockmask.size)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Cold-air outbreaks (CAOs)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plevqs = [('sfc','msl'), ('sfc', 'sst'), ('sfc','siconc'), ('850','t')]\n",
    "dat, grid = get_instantaneous(plevqs, period)\n",
    "\n",
    "# Load land-sea mask; currently not automatically loaded into the grid object. This might become just grid.lsm in the future.\n",
    "f, lsm = metopen(conf.staticfile, 'lsm', no_static=True)\n",
    "lsm = lsm.squeeze()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "caoidx = dynlib.detect.cold_air_outbreak_index(dat['850','t'].squeeze(), dat['sfc','msl'], dat['sfc','sst'], \n",
    "                                               dat['sfc','siconc'], lsm)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Here the returned CAO index field marks the strength of the cold-air outbreak in Kelvin, or zero if no CAO is detected. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(caoidx.shape)                          # shape (t,y,x)\n",
    "print(np.nanmax(caoidx))                     # Maximum CAO-index in K\n",
    "print((caoidx >= 4).sum()/caoidx.size)       # Fraction of grid points with at least moderate CAO conditions"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Note that the caoidx field contains many NaNs. This is because the SST-based index used here is not defined over land or sea ice. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(np.isnan(caoidx).sum()/caoidx.size)    # Fraction of grid points containing NaN values"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Plotting everything"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": false
   },
   "outputs": [],
   "source": [
    "%matplotlib inline\n",
    "\n",
    "# Workaround for a bug in dynlib, the line array is returned as float64 although it should be/ only contains integers\n",
    "mtaoff = mtaoff.astype('i4')\n",
    "jaoff = jaoff.astype('i4')\n",
    "\n",
    "def plot(tidx):\n",
    "    title = grid.t_parsed[tidx].strftime('%Y-%m-%d %HZ')\n",
    "    fig.setup(fig_size=30, title=title)\n",
    "    overlays = [\n",
    "        # Cyclones in transparent cyan shading\n",
    "        fig.map_overlay_shading(cycmask[tidx,:,:] > 0.5, grid, scale=[0.5,2], colors=['cyan',], extend='neither', \n",
    "                                cmap=None, cb_disable=True, alpha=0.3),\n",
    "        # Anticyclones in transparent orange shading\n",
    "        fig.map_overlay_shading(acycmask[tidx,:,:] > 0.5, grid, scale=[0.5,2], colors=['darkorange',], extend='neither', \n",
    "                                cmap=None, cb_disable=True, alpha=0.3),\n",
    "        # Frontal volumes at 850 hPa (second vertical level) in transparent dark blue shading\n",
    "        fig.map_overlay_shading(frovo_id[tidx,1,:,:] > 0.5, grid, scale=[0.5,2], colors=['navy',], extend='neither', \n",
    "                                cmap=None, cb_disable=True, alpha=0.4),\n",
    "        # Moderate-to-strong CAOs in transparent light blue shading\n",
    "        fig.map_overlay_shading(caoidx[tidx,:,:] > 4, grid, scale=[0.5,2], colors=['lightskyblue',], extend='neither', \n",
    "                                cmap=None, cb_disable=True, alpha=0.5),\n",
    "        # MTAs: white-yellow lines\n",
    "        fig.map_overlay_lines(mtaxis[tidx,:,:], mtaoff[tidx,:], grid, linecolor='w', linewidth=4),\n",
    "        fig.map_overlay_lines(mtaxis[tidx,:,:], mtaoff[tidx,:], grid, linecolor='gold', linewidth=2),\n",
    "        # Rossby wave breaking\n",
    "        fig.map_overlay_shading(rwb_a[tidx,:,:]+rwb_c[tidx,:,:], grid, scale=[0.5,2], colors=['firebrick',], extend='neither', \n",
    "                                cmap=None, cb_disable=True, alpha=0.5),        \n",
    "        # Jet axes: black-white lines\n",
    "        fig.map_overlay_lines(ja[tidx,:,:], jaoff[tidx,:], grid, linecolor='k', linewidth=5),\n",
    "        fig.map_overlay_lines(ja[tidx,:,:], jaoff[tidx,:], grid, linecolor='w', linewidth=3),\n",
    "    ]\n",
    "    fig.map(dat['sfc','msl'][tidx,:,:]/100, grid, overlays=overlays, title=title, \n",
    "            scale=np.arange(950,1051,5), m=proj.N_Atlantic, orocolor=None, cmap='Greys_r')\n",
    "    return\n",
    "\n",
    "for tidx in [0, 8, 16, 24, 32, 40, 48, 56]:\n",
    "    plot(tidx)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "dynpie3-2021a",
   "language": "python",
   "name": "dynpie3-2021a"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.0"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}

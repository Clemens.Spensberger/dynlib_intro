{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Dynlib introduction: Define and calculate composites\n",
    "\n",
    "In this notebook we will: \n",
    " - Create an NAO+ composite based on ERA5 data\n",
    " - Define a data-driven composite for warm summer days in Bergen\n",
    " - Plot the results\n",
    " - Save the results back to disk\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from dynlib.metio.era5 import conf, dt, metopen, get_composite, get_static, metsave_composite\n",
    "from dynlib.metio.composite import decide_by_date, decide_by_data, decide_by_timeseries, matrix"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In dynlib, calculating composites is treated as a special case of reading data (i.e. selective reading). The relevant functions are thus part of ``dynlib.metio``.\n",
    "\n",
    "Like in the general data request we need to define the variables and time period we want to request the composit for."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Which variable(s) to composite, given by list of 2-tuples (plev, q)\n",
    "plevqs = [('500', 'z'), ]\n",
    "\n",
    "# Time interval to consider for the composites, here 2001-2010\n",
    "timeinterval = [dt(2001,1,1,0), dt(2011,1,1,0)]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## NAO composites"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now we need to define the criteria for when a time step is to be included in the composite. As you can already see in the import command, dynlib provides different kinds of criteria. The first we'll consider is ``decide_by_date``."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "djf = decide_by_date('DJF', lambda date: date.month in [12,1,2])\n",
    "jja = decide_by_date('JJA', lambda date: date.month in [6,7,8]) "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As you can see, defining such criteria is easy. The function ``decide_by_date`` just takes a name, and then a function that returns either ``True`` or ``False`` for any given date. \n",
    "\n",
    "Next we'll consider ``decide_by_timeseries`` which works very much in the same way."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ts = metopen('indexes/ts_NAO', no_static=True)\n",
    "naop = decide_by_timeseries('NAO+', ts, lambda idx: idx >= 1.0)\n",
    "naom = decide_by_timeseries('NAO-', ts, lambda idx: idx <= -1.0)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If you have a look into ``/Data/gfi/users/local/share/indexes`` you'll see that many other climate variability indices are available there, ready to be used in your composites.\n",
    "\n",
    "A small digression: It is also quite easy to define one's one index time series. Just have a look at what is in the ``npz``-file that we opened:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "list(ts.keys())"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "It is just two numpy arrays, one called dates and one called valued."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print('Dates')\n",
    "print(len(ts['dates']), ts['dates'][:5], ts['dates'][-5:])\n",
    "print()\n",
    "print('Values')\n",
    "print(len(ts['values']), ts['values'][:5], ts['values'][-5:])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "So you can easily define your own index time series by just saving your data in this format."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Return from the digression. Now we want to combine the seasonal and the NAO criteria, calculating a NAO+/- composite for summer and winter. You can do this by using boolean operators:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "naop_djf = naop & djf\n",
    "print(naop_djf.name)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This will both combine the criteria and define a suggestive name for the new composite.\n",
    "\n",
    "Often enough one wants all possible combinations of different criteria, which can be a bit tedious to define manually as above. That's why there is the shortcut ``matrix``."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "composites = matrix([naom, naop], [djf, jja,])\n",
    "print([composite.name for composite in composites])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Note that the order of the arguments to ``matrix`` defines how the resulting composites will be named, and also the grouping of the variables in the netCDF file if saved."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "composites2 = matrix([djf, jja,], [naom, naop])\n",
    "print([composite.name for composite in composites2])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Time to actually request and calculate the composites."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "dat = get_composite(plevqs, timeinterval, composites)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As you can see from the output, only those months that are required for any of the composites are actually read. What did we get as a result?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(list(dat.keys()))\n",
    "print(list(dat['NAO-@DJF', '500', 'z'].keys()))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "A dictionary with keys as 3-tuple consisting of the composite name, vertical level and variable. And for each composite and variable, we get the composite mean, the within-composite standard deviation as well as the number of time steps with valid data for each grid point.\n",
    "\n",
    "There is a convenience function for saving data in this format, ``metsave_composite``."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Save the composites as one or more netCDF file(s) \n",
    "\n",
    "# We didn't get the grid from get_composites, so requesting one here manually.\n",
    "grid = get_static()\n",
    "\n",
    "metsave_composite(dat, composites, grid, 'ea.ans.2001-2010.NAO_composites')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Data-driven composites: warm summer days in Bergen\n",
    "\n",
    "If you want to define a composite for all days where 2-metre temperatures in Bergen exceed a threshold, you can of course extract the relevant data from ERA5 and define a index time series. But for such a case in which the composite criteria is based immediately on the underlying reanalysis, there is an easier way, ``decide_by_data``."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "warm_bgo = decide_by_data('warm_BGO', 'sfc', 't2m', lambda t2m: t2m[60,370] > 293.0)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The pattern is still very much the same as before, but in addition to name and the function defining the criterion, ``decide_by_data`` takes the vertical level and variable name as additional arguments. \n",
    "\n",
    "The lambda-function in the final argument does now get the full requested data field for one time step and needs to return either ``True`` or ``False`` based on the given data. Here, it just takes the grid point corresponding to Bergen (60°N / 5°E) and checks if the temperatures exceed 20°C."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Only interested in warm days during summer:\n",
    "composites = [warm_bgo & jja, ]\n",
    "dat = get_composite(plevqs, timeinterval, composites)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "metsave_composite(dat, composites, grid, 'ea.ans.2001-2010.warm_BGO_composite')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Finally, some plotting."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import dynlib.figures as fig\n",
    "import dynlib.proj as proj\n",
    "import numpy as np"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%matplotlib inline\n",
    "\n",
    "z500 = dat['warm_BGO_sfc_t2m@JJA','500','z']['mean'][0,:,:]\n",
    "overlays = [\n",
    "    fig.map_overlay_contour(z500, grid, q='z', plev='500', scale=[552,], \n",
    "                            hook=lambda z: z/98.1, colors='white')\n",
    "]\n",
    "fig.map(z500, grid, q='z', plev='500', m=proj.N_Atlantic_forecast,\n",
    "        scale=np.arange(476,601,4), hook=lambda z: z/98.1, cmap='magma', \n",
    "        ticks=np.arange(480,601,20), cb_orientation='vertical', overlays=overlays,\n",
    ")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "dynpie3-2021a",
   "language": "python",
   "name": "dynpie3-2021a"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.0"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}

{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Dynlib introduction: Combining data from several sources\n",
    "\n",
    "In this notebook we will: \n",
    " - Use both NeXtSIM and ERA5 data at the same time\n",
    " - Interpolate ERA5 data onto the NeXtSIM grid (using ``scipy.interpolate``)\n",
    " - Relate changes in sea ice to sea-level pressure (or not :))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "import scipy.interpolate as interp\n",
    "\n",
    "from dynlib.metio import nextsim, era5"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The main \"trick\" to use several datasets at once is the above: importing the two data sources into their separate namespaces, such that all data retrieval functions for both data sets remain available. Then we can simply ... "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# What to request for which time interval\n",
    "# Our NORA10 data repository follows ECMWF conventions, so we can use the same plevqs for both datasets.\n",
    "plevqs_e5 = [('sfc','msl'), ]\n",
    "plevqs_nx = [('sfc','yiv_tend_thermo')]\n",
    "timeinterval = [era5.dt(2011,12,25,0), era5.dt(2011,12,26,12)]\n",
    "\n",
    "dat_nx, grid_nx = nextsim.get_instantaneous(plevqs_nx, timeinterval)\n",
    "dat_e5, grid_e5 = era5.get_instantaneous(plevqs_e5, timeinterval)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Have a look around. What did we get?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(dat_nx.keys())\n",
    "print(dat_e5.keys())"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(dat_nx['sfc','yiv_tend_thermo'].shape)\n",
    "print(dat_e5['sfc','msl'].shape)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Note the different number of time steps (first dimension)! For NeXtSIM we have 6-hourly data, and 3-hourly for ERA5."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(grid_nx.x.min())"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Note that there are NaN values in the grid_nx grid definitions. How can that be? The following plots showing where the NaNs occur should make this clear!"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import matplotlib.pyplot as plt\n",
    "\n",
    "%matplotlib inline\n",
    "\n",
    "plt.imshow(np.isnan(grid_nx.x))\n",
    "plt.colorbar()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(np.nanmin(grid_nx.y), np.nanmax(grid_nx.y))\n",
    "print(grid_e5.y.min(), grid_e5.y.max())"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "\n",
    "And in both cases, we have latitude and longitude information for the grid in ``grid.y`` and ``grid.x``. That we will make use of for the horizontal interpolation, powered by ``scipy.interpolate``."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Allocate empty array for the results\n",
    "e5_on_nx = np.empty(dat_nx['sfc','yiv_tend_thermo'].shape)\n",
    "\n",
    "# Interate over time steps\n",
    "for tidx_nx in range(dat_nx['sfc','yiv_tend_thermo'].shape[0]):\n",
    "    # Every second time step in ERA-5 corresponds to the NeXtSIM time steps\n",
    "    tidx_e5 = tidx_nx * 2\n",
    "    \n",
    "    # Generate the interpolation function for the data that is to be interpolated (ERA5)\n",
    "    dat_ = dat_e5['sfc','msl'][tidx_e5,...]\n",
    "    ifunc = interp.RectBivariateSpline(grid_e5.y[::-1,0], grid_e5.x[0,:], dat_[::-1,:], kx=1, ky=1)\n",
    "    \n",
    "    # Evaluate the interpolation function at the lats/lons of the NeXtSIM grid\n",
    "    e5_on_nx[tidx_nx,...] = ifunc(grid_nx.y, grid_nx.x, grid=False)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "A few notes on the interpolation.\n",
    "\n",
    " - ``interp.RectBivariateSpline`` assumes the input grid to be regular, thus ``grid.y`` and ``grid.x`` are passed on as one-dimensional arrays.\n",
    " - The arguments ``kx`` and ``ky`` give the order of the interpolation, here bi-linear. \n",
    " - The keyword argument ``grid=False`` when evaluating is important. By default ``grid=True``, in which case all possible combinations of the input ``grid.y`` and ``grid.x`` are being calculated."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "But now that we have the sea-level pressure on the same grid, we can simply plot the differences between ERA5 and NORA10."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import dynlib.figures as fig\n",
    "import dynlib.proj as proj\n",
    "\n",
    "%matplotlib inline\n",
    "\n",
    "for tidx in [0, 2, 4]: # plot only a few selected time steps\n",
    "    # Overlay ERA5 SLP in contours\n",
    "    overlays = [\n",
    "        fig.map_overlay_contour(e5_on_nx[tidx,...], grid_nx, q='msl', plev='sfc', \n",
    "                scale=np.arange(94000,105001,1000), linewidths=1, alpha=0.6),\n",
    "    ]\n",
    "    # ... and young ice thermodynamic tendency as shading.\n",
    "    fig.map(dat_nx['sfc','yiv_tend_thermo'][tidx,:,:], grid_nx, q='msl', plev='sfc', title=f'NeXtSIM yiv_tend_thermo/ERA5 SLP for {grid_nx.t_parsed[tidx]}', \n",
    "            m=proj.atlantic_arctic, cmap=\"RdBu_r\",\n",
    "            cb_orientation='vertical', overlays=overlays,\n",
    "    )\n",
    "    fig.plt.close()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "And now that we have the two fields on one plot we can run simple analyses on them to see if there is any relation between the two?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Find those places where the young ice is increasing\n",
    "somechange = dat_nx['sfc','yiv_tend_thermo'] > 0.005\n",
    "\n",
    "# Did this sea-ice increase occur at specific MSL values?\n",
    "plt.hexbin(e5_on_nx[somechange], dat_nx['sfc','yiv_tend_thermo'][somechange], mincnt=10, cmap='Blues', vmin=0)\n",
    "plt.colorbar()\n",
    "plt.ylabel('NeXtSIM Young ice thermodynamic growth')\n",
    "plt.xlabel('ERA5 sea-level pressure')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "dynpie3-2021a",
   "language": "python",
   "name": "dynpie3-2021a"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.0"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}

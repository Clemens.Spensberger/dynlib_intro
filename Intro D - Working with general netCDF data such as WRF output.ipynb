{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Dynlib introduction: Working with general netCDF data such as WRF output\n",
    "\n",
    "In this notebook we will: \n",
    " - Read WRF output\n",
    " - Interpolate in the vertical from WRF hybrid levels to isobaric and Cartesian coordinates\n",
    " - Plot the results\n",
    " - Save the results back to disk"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Reading generic netCDF files"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from dynlib.metio.generic import metopen, metsave, conf, dt"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "With the dataset unknown to dynlib, many convenience functions such as ``get_instantaneous`` that allow to directly request specific time slices are not available. Nevertheless, we can read and write generic netCDF data using ``metopen`` and ``metsave``. \n",
    "\n",
    "One of the advantages of reading/ writing data this way is ``conf.datapath``, a list of directories in which dynlib looks for data, and ``conf.opath`` the default output directory. Defining these directory in a central place makes life easier."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Print defaults\n",
    "print(conf.datapath)\n",
    "print(conf.opath)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Contains WRF output from a simulation by Dandan Tao, a cyclone moving over ice\n",
    "conf.datapath.insert(1, '/Data/gfi/work/csp001/wrf_example')\n",
    "# Set this to something sensible, you will not be able to write to this folder ;-)\n",
    "conf.opath = '/Data/gfi/work/csp001/wrf_example_postprocessed'"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "With these paths properly set we can proceed and see what ``metopen`` can do for us."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "f = metopen('wrfout_d01_2000-01-02_00:00:00', no_static=True)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In the simplest configuration, metopen will only return a netCDF4 file object. For comprehensive documentation on what this object can do, refer to the documentation for the ``netCDF4``-package. But some useful things are summarised here:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Access variables.\n",
    "print(f.variables['T2'])\n",
    "print()\n",
    "\n",
    "# Access global attributes\n",
    "print(f.TITLE)\n",
    "print()\n",
    "\n",
    "# Access variable attributes\n",
    "print(f.variables['T2'].units)\n",
    "print()\n",
    "\n",
    "# Access variable data as numpy array\n",
    "print(type(f.variables['T2'][...]))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "But often enough we want to request a specific variable from a netCDF file. This is possible through an optional second argument to ``metopen``."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "f, dat, grid = metopen('wrfout_d01_2000-01-02_00:00:00', 'T2')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The function now returns three arguments: ``f`` is the netCDF4 file object we explored above, ``dat`` is a numpy array for the data we requested, and ``grid`` is the grid information corresponding to the requested data."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(type(dat), dat.shape)\n",
    "print(grid.y.min(), grid.y.max())\n",
    "print(grid.dy.min(), grid.dy.max())"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "With the data and grid object available, we can directly use all pretty much all of dynlib's diagnostics and detection algorithms. And we can save the data back to disk."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Often you can also request a grid object without a variable specified. For WRF output this does not work:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "f, grid = metopen('wrfout_d01_2000-01-02_00:00:00')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The reason is that WRF output contains several horizontal grids, so dynlib would not know which of them to return. With a variable requested, there is no more ambiguity, and dynlib returns the grid corresponding to the requested variable."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "With these basics in place, we can now proceed to \n",
    "\n",
    "## Vertical interpolation of WRF output"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "import dynlib.interpol"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# We'll request a few variables from the same file, so best to define it somewhere in one place\n",
    "fname = 'wrfout_d01_2000-01-02_00:00:00'\n",
    "\n",
    "f, qv, grid = metopen(fname, 'QVAPOR')\n",
    "print(qv.shape)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "For interpolating to pressure levels, we need 3-dimensional pressure array. In WRF the sum of ``P`` (dynamic variable pressure) and ``PB`` (basic-state pressure) give the full pressure field."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "f, p_dyn = metopen(fname, 'P', no_static=True)\n",
    "f, p_basic = metopen(fname, 'PB', no_static=True)\n",
    "pres = p_dyn + p_basic\n",
    "print(pres.shape, pres.min(), pres.max())\n",
    "\n",
    "# Some clean-up to free memory\n",
    "del p_dyn, p_basic"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Interpolate to the 850 hPa and 925 hPa levels.\n",
    "# Levels listed here must be in decreasing order, the order in which the levels are occurring in the pres array.\n",
    "plevs = np.array([92500.0, 85000.0, ])\n",
    "qv_on_p = dynlib.interpol.vert_by_coord(qv, pres, plevs)\n",
    "print(qv_on_p.shape, qv_on_p.min(), qv_on_p.max())"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Interpolation to Cartesian height coordinates works along the same lines. We only need to replace the ``pres`` and ``plevs`` arrays with ones containing Cartesian heights. We derive them here from the geopotential in the WRF output."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "f, z_dyn = metopen(fname, 'PH', no_static=True)\n",
    "f, z_basic = metopen(fname, 'PHB', no_static=True)\n",
    "z = (z_dyn + z_basic)/9.81\n",
    "print(z.shape, z.min(), z.max())\n",
    "\n",
    "# Some clean-up to free memory\n",
    "del z_dyn, z_basic"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As we can see from the shape, ``z`` is defined on vertically staggered levels. So we need to interpolate to the main grid first. Fortunately this is a simple geometric mean:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "z = (z[:,1:,:,:] + z[:,:-1,:,:])/2.0\n",
    "print(z.shape, z.min(), z.max())"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now that looks better!"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Interpolate to the 1500 m-levels.\n",
    "# Levels listed here must be in increasing order, the order in which the levels are occurring in the pres array.\n",
    "zlevs = np.array([1500.0, ])\n",
    "qv_on_z = dynlib.interpol.vert_by_coord(qv, z, zlevs)\n",
    "print(qv_on_z.shape, qv_on_z.min(), qv_on_z.max())"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Finally, some plotting!"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import matplotlib.pyplot as plt\n",
    "import dynlib.cm as cm\n",
    "\n",
    "%matplotlib inline"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Plot only slice of the domain in x to focus on the interesting part\n",
    "xslc = slice(600,1000)\n",
    "\n",
    "plt.figure(figsize=(8,4))\n",
    "plt.contourf(grid.x[0,xslc]/1000, grid.y[:,0]/1000, qv_on_z[0,:,xslc]*1000, \n",
    "        np.arange(0, 5.1, 0.5), extend='max', cmap=cm.WYlBuKPi)\n",
    "plt.colorbar()\n",
    "plt.gca().set_aspect('equal')\n",
    "plt.title(\"Specific humidity at 1500 m [g/kg]\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plt.figure(figsize=(8,4))\n",
    "plt.contourf(grid.x[0,xslc]/1000, grid.y[:,0]/1000, qv_on_p[1,:,xslc]*1000, \n",
    "        np.arange(0, 5.1, 0.5), extend='max', cmap=cm.WYlBuKPi)\n",
    "plt.colorbar()\n",
    "plt.gca().set_aspect('equal')\n",
    "plt.title(\"Specific humidity at 850 hPa [g/kg]\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "dynpie3-2021a",
   "language": "python",
   "name": "dynpie3-2021a"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.0"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
